<!DOCTYPE html>
<?php include("fonction/fonction.php"); ?>
<?php
if(isset($_GET['categorie']) AND isset($_GET['sous_categorie']) AND isset($_GET['id_nouvelle'])) {
	
	if(CountNouvelleId($_GET['id_nouvelle'])>0) {
		$categorie = $_GET['categorie'];
		$sous_categorie = $_GET['sous_categorie'];
		$id_nouvelle = $_GET['id_nouvelle']; 
		
	} else {
		header('Location: '.url().'Accueil');
		exit();
	}

} else {
	header('Location: '.url().'Accueil');
	exit();
}
///COMMENTAIRE
if(isset($_POST['commentaire']) AND !empty($_POST['commentaire']) AND isset($_POST['nom']) AND !empty($_POST['nom']) AND isset($_POST['email']) AND !empty($_POST['email'])) {
	
	$commentaire = $_POST['commentaire'];
	$nom = $_POST['nom'];
	$email = $_POST['email'];
	
	if (filter_var($email, FILTER_VALIDATE_EMAIL)) { //Validation mail
					$ok_mail=1;
	} else {
		$erreur = "Adresse email non valide";
	}
	if(isset($ok_mail)){
		InsertCommentaire($id_nouvelle,$commentaire,$nom,$email);
	}

} else if(isset($_POST['commentaire']) AND empty($_POST['commentaire']) OR isset($_POST['nom']) AND empty($_POST['nom']) OR isset($_POST['email']) AND empty($_POST['email'])) {
	$erreur = "Tout les champs sont obligatoire";
}
$req = TrouvNouvelle($id_nouvelle);
$donnees = $req->fetch();
?>
<html lang="en">
<head>
	<title>MAGNEWS : <?php echo $donnees['titre'];  ?></title>
	<?php echo RacineHref(); ?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo premiers_mots(36,$donnees['description']); $req->closeCursor(); ?>...">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" href="concatcss/" type="text/css" media="screen">
		<link rel="stylesheet" type="text/css" href="fonts/fontawesome-5.0.8/css/fontawesome-all.min.css">
</head>
<body class="animsition">
	
	<!-- Header -->
		<?php include("include/header.php"); ?>
	<!-- Breadcrumb -->
	<?php 
		$req = TrouvNouvelle($id_nouvelle);
		$donnees = $req->fetch();
	?>
	<div class="container">
		<div class="headline bg0 flex-wr-sb-c p-rl-20 p-tb-8">
			<div class="f2-s-1 p-r-30 m-tb-6">
				<a href="Accueil" class="breadcrumb-item f1-s-3 cl9">
					<?php echo $categorie; ?> 
				</a>

				<a href="Categorie/<?php echo $categorie; ?>/<?php echo $sous_categorie; ?>" class="breadcrumb-item f1-s-3 cl9">
					<?php echo $sous_categorie; ?> 
				</a>

				<span class="breadcrumb-item f1-s-3 cl9">
					 <?php echo $donnees['titre']; ?> 
				</span>
			</div>

				<form class="pos-relative size-a-2 bo-1-rad-22 of-hidden bocl11 m-tb-6" action="Recherche" method="POST">
				<input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45" type="text" name="recherche" placeholder="Recherche">
				<button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
					<i class="zmdi zmdi-search"></i>
				</button>
			</form>
		</div>
	</div>

	<!-- Content -->
	<section class="bg0 p-b-140 p-t-10">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 col-lg-8 p-b-30">
					<div class="p-r-10 p-r-0-sr991">
						<!-- Blog Detail -->
						
						<div class="p-b-70">
							<a href="#" class="f1-s-10 cl2 hov-cl10 trans-03 text-uppercase">
								<?php echo $sous_categorie; ?> 
							</a>

							<h3 class="f1-l-3 cl2 p-b-16 p-t-33 respon2">
								 <?php echo $donnees['titre']; ?> 
							</h3>
							
							<div class="flex-wr-s-s p-b-40">
								<span class="f1-s-3 cl8 m-r-15">
									<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
										par <?php echo $donnees['auteur']; ?> 
									</a>

									<span class="m-rl-3">-</span>

									<span>
										<?php echo DateMoiAnsJour($donnees['date_nouvelle']); ?>
									</span>
								</span>

								<span class="f1-s-3 cl8 m-r-15">
									<?php echo $donnees['vue']; ?>  Views
								</span>

								<a href="#" class="f1-s-3 cl8 hov-cl10 trans-03 m-r-15">
									<?php echo CountCommentaire($id_nouvelle); ?> Commentaire
								</a>
							</div>

							<div class="wrap-pic-max-w p-b-30">
								<img src="nouvelle/<?php echo $donnees['id_nouvelle']; ?>.jpg" alt="IMG">
							</div>

							<p class="f1-s-11 cl6 p-b-25">
								<?php echo $donnees['description']; ?>
							</p>

							<!--<p class="f1-s-11 cl6 p-b-25">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sit amet est vel orci luctus sollicitudin. Duis eleifend vestibulum justo, varius semper lacus condimentum dictum. Donec pulvinar a magna ut malesuada. In posuere felis diam, vel sodales metus accumsan in. Duis viverra dui eu pharetra pellentesque. Donec a eros leo. Quisque sed ligula vitae lorem efficitur faucibus. Praesent sit amet imperdiet ante. Nulla id tellus auctor, dictum libero a, malesuada nisi. Nulla in porta nibh, id vestibulum ipsum. Praesent dapibus tempus erat quis aliquet. Donec ac purus id sapien condimentum feugiat.
							</p>

							<p class="f1-s-11 cl6 p-b-25">
								Praesent vel mi bibendum, finibus leo ac, condimentum arcu. Pellentesque sem ex, tristique sit amet suscipit in, mattis imperdiet enim. Integer tempus justo nec velit fringilla, eget eleifend neque blandit. Sed tempor magna sed congue auctor. Mauris eu turpis eget tortor ultricies elementum. Phasellus vel placerat orci, a venenatis justo. Phasellus faucibus venenatis nisl vitae vestibulum. Praesent id nibh arcu. Vivamus sagittis accumsan felis, quis vulputate
							</p> -->

							<!-- Tag -->
							<div class="flex-s-s p-t-12 p-b-15">
								<span class="f1-s-12 cl5 m-r-8">
									Tags:
								</span>
								
								<div class="flex-wr-s-s size-w-0">
									<a href="#" class="f1-s-12 cl8 hov-link1 m-r-15">
										Streetstyle
									</a>

									<a href="#" class="f1-s-12 cl8 hov-link1 m-r-15">
										Crafts
									</a>
								</div>
							</div>
							
							<!-- Share -->
							<div class="flex-s-s">
								<span class="f1-s-12 cl5 p-t-1 m-r-15">
									Share:
								</span>
								
								<div class="flex-wr-s-s size-w-0">
									<a href="#" class="dis-block f1-s-13 cl0 bg-facebook borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-facebook-f m-r-7"></i>
										Facebook
									</a>

									<a href="#" class="dis-block f1-s-13 cl0 bg-twitter borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-twitter m-r-7"></i>
										Twitter
									</a>

									<a href="#" class="dis-block f1-s-13 cl0 bg-google borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-google-plus-g m-r-7"></i>
										Google+
									</a>

									<a href="#" class="dis-block f1-s-13 cl0 bg-pinterest borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-pinterest-p m-r-7"></i>
										Pinterest
									</a>
								</div>
							</div>
						</div>
						<h4 class="f1-l-4 cl3 p-b-12">
								Commentaire
							</h4>
						<div>
							<?php $req_commentaire = ListCommentaire($id_nouvelle);
								while($donnees_commentaire = $req_commentaire->fetch()) {
							?>
							<div>
								<div class="cl8 p-b-18">
								<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">Par <?php echo $donnees_commentaire['nom']; ?></a>
								<span class="f1-s-3 m-rl-3">-</span>
								<span class="f1-s-3"><?php echo DateMoiAnsJour($donnees_commentaire['date_commentaire']); ?></span>
								</div>
								<p class="f1-s-1 cl6 p-b-24">
								<?php echo $donnees_commentaire['commentaire']; ?>
								</p>
							</div>
							<?php } $req_commentaire->closeCursor(); ?>
							<!-- Leave a comment -->
						</div>
						<div>
							

							<p class="f1-s-13 cl8 p-b-40">
								Votre email ne sera pas publier. Tous les champs sont obligatoire *
							</p>

							<form method="POST">
								<textarea class="bo-1-rad-3 bocl13 size-a-15 f1-s-13 cl5 plh6 p-rl-18 p-tb-14 m-b-20" name="commentaire" placeholder="Commentaire..."></textarea>

								<input class="bo-1-rad-3 bocl13 size-a-16 f1-s-13 cl5 plh6 p-rl-18 m-b-20" type="text" name="nom" placeholder="Nom*">

								<input class="bo-1-rad-3 bocl13 size-a-16 f1-s-13 cl5 plh6 p-rl-18 m-b-20" type="text" name="email" placeholder="Email*">

							<p class="f1-s-13 cl8">
								<?php if(isset($erreur))  { echo $erreur; } ?>
 							</p>
								<button class="size-a-17 bg2 borad-3 f1-s-12 cl0 hov-btn1 trans-03 p-rl-15 m-t-10">
									Post Comment
								</button>
							</form>
						</div>
					</div>
				</div>
				
				<!-- Sidebar -->
				<!-- Sidebar -->
				<div class="col-md-5 col-lg-4 p-b-30">
					<div class="p-l-10 p-rl-0-sr991 p-t-5">
						<!-- Popular Posts -->
						<div>
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Les plus populaire
								</h3>
							</div>

							<ul class="p-t-35">
								<?php $nb_top=1; $req_top_nouvelle = ListNouvelleNouveauLimit(5); while ($donnees_topnl = $req_top_nouvelle->fetch()) { //LIST DE CATEGORIE ?>
							
								<li class="flex-wr-sb-s p-b-22">
									<div class="size-a-8 flex-c-c borad-3 size-a-8 bg9 f1-m-4 cl0 m-b-6">
										<?php echo $nb_top; ?>
									</div>

									<a href="Categorie/<?php echo $donnees_topnl['categorie'].'/'.$donnees_topnl['sous_categorie'].'/'.$donnees_topnl['id_nouvelle']; ?> " class="size-w-3 f1-s-7 cl3 hov-cl10 trans-03">
										<?php echo $donnees_topnl['titre']; ?>
									</a>
								</li>
								<?php $nb_top++; } $req_top_nouvelle->closeCursor(); ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	
	<?php 
		
		ModifierView($id_nouvelle);
		$req->closeCursor();
		
	?>
	<!-- Footer -->
		<?php include("include/footer.php"); ?>
	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<span class="fas fa-angle-up"></span>
		</span>
	</div>


<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>