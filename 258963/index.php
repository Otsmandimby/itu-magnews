<!DOCTYPE html>
<?php 
session_start();
if(isset($_SESSION['utilisateur']))
{
	header('Location: ajouter.php');
	exit();
}
//categorie sous categorie
$menu=array("Freinage","Filtration","Embrayages/Boite/Transmission","Compartiment moteur"); 
$smenu["Freinage"]=array("Freins a disque","Systme de freinage");
$smenu["Filtration"]=array("Filtre","Huiles/Vidanges");
$smenu["Embrayages/Boite/Transmission"]=array("Embrayages","Cardan");
$smenu["Compartiment moteur"]=array("Courroie","Distribution");

include("../fonction/connect_pdo.php");
include("../fonction/fonction.php");


?>
<?php 
if(isset($_POST['utilisateur']) AND !empty($_POST['utilisateur']) AND isset($_POST['mot_de_passe']) AND !empty($_POST['mot_de_passe'])) {
	
	if($_POST['utilisateur']=="ITU-magnews" and $_POST['mot_de_passe']==12378955) {
		echo "login";
		$_SESSION['utilisateur']="ITU-magnews";
		header('Location: ajouter.php');
	} else {
		$erreur="Non d'utilisateur ou mot de passe invalide";
	}
}
else if(isset($_POST['utilisateur']) AND empty($_POST['utilisateur']) or isset($_POST['mot_de_passe']) AND empty($_POST['mot_de_passe'])) {
	$erreur="Tout les champs sont obligatoire";
} ?>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MAGNEWS: Admin - Se connecter</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper" style="padding-left: 0;">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">MAGNEWS Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               
                <li class="dropdown">
                   
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            SE CONNECTER 

                        </h1>
                    </div>
                </div>
                <!-- /.row -->
				<div class="row">
					<div class="col-lg-1">
						
					</div>
					<div class="col-lg-10">
						<form method="POST" name="login">
						
							 <div class="form-group">
                                <label>Utilisateur:</label>
                                <input name="utilisateur" class="form-control"/>
                            </div>
							<div class="form-group">
                                <label>Mot de passe:</label>
                                <input name="mot_de_passe" class="form-control"/>
                            </div>
                              <button type="submit" onclick="login.submit()" class="btn btn-primary">AJOUTER</button>
                           
						</form>
						
						<div class="form-group has-error">
                                <label class="control-label" for="inputError"><?php if(isset($erreur)) { echo $erreur;  } ?></label>
                              
                            </div>
					</div>
					<div class="col-lg-1"></div>
				</div>
               
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
