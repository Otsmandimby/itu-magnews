<?php
session_start();
if(isset($_SESSION['utilisateur']))
{
	session_destroy();
	header('Location: index.php');
} else if(!isset($_SESSION['utilisateur'])) {
	header('Location: index.php');	
}
?>