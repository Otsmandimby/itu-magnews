<!DOCTYPE html>
<?php 
session_start(); 
if(!isset($_SESSION['utilisateur']))
{
	header('Location: index.php');
	exit();
}
include("../fonction/fonction.php");
if(isset($_POST['id_nouvelle'])) {
	$id_nouvelle = $_POST['id_nouvelle'];
	SuprNouvelle($id_nouvelle);
}
					
?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

   <title>MAGNEWS: Admin - suprimer nouvelle</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">MAGNEWS Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Onintsoa<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="deconnect.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
				
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                  
                    <li  class="active">
                        <a href="suprimer.php"><i class="fa fa-fw fa-table"></i> Suprimer nouvelle</a>
                    </li>
                    <li>
                        <a href="ajouter.php"><i class="fa fa-fw fa-edit"></i> Ajouter nouvelle</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Nouvelles
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a>Suprimer</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> Nouvelle
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				
                <div class="row">
					
                    <div class="col-lg-8">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Id</th>
										 <th>Categorie</th>
										  <th>Sous_categorie</th>
                                        <th>Auteur</th>
                                        <th>Titre</th>
                                        <th>Nombre de vue</th>
										 <th>Date</th>
										 <th>Suprimer</th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php
										$reponse = ListNouvelle();
										while ($donnees = $reponse->fetch()) //$donnees = $reponse->fetch();
										{
											
										
									?>
									<tr>
                                        <td><?php echo $donnees['id_nouvelle']; ?></td>
                                        <td><?php echo $donnees['categorie']; ?></td>
                                        <td><?php echo $donnees['sous_categorie']; ?></td>
                                        <td><?php echo $donnees['auteur']; ?></td>
                                        <td><?php echo $donnees['titre']; ?></td>
                                        <td><?php echo $donnees['vue']; ?></td>
                                        <td><?php echo $donnees['date_nouvelle']; ?></td>
										<td><form method="POST" name="suprimer<?php echo $donnees['id_nouvelle']; ?>"><input type="hidden" name="id_nouvelle" value="<?php echo $donnees['id_nouvelle']; ?>"/><button type="button" onclick="suprimer<?php echo $donnees['id_nouvelle']; ?>.submit()" class="btn btn-lg btn-danger">SUPRIMER</button></form></td>
                                    </tr>
									<?php } $reponse->closeCursor(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
