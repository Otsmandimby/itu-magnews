<!DOCTYPE html>
<?php
session_start(); 
if(!isset($_SESSION['utilisateur']))
{
	header('Location: index.php');
	exit();
}
//categorie sous categorie
$menu=array("Freinage","Filtration","Embrayages/Boite/Transmission","Compartiment moteur"); 
$smenu["Freinage"]=array("Freins a disque","Système de freinage");
$smenu["Filtration"]=array("Filtre","Huiles/Vidanges");
$smenu["Embrayages/Boite/Transmission"]=array("Embrayages","Cardan");
$smenu["Compartiment moteur"]=array("Courroie","Distribution");

include("../fonction/connect_pdo.php");
include("../fonction/fonction.php");


?>
<?php 
if(isset($_POST['categorie']) AND !empty($_POST['categorie']) AND $_POST['categorie']!="Tout" AND isset($_POST['sous_categorie']) AND !empty($_POST['sous_categorie']) AND $_POST['sous_categorie']!="Tout" AND isset($_POST['auteur']) AND !empty($_POST['auteur']) AND isset($_POST['titre']) AND !empty($_POST['titre']) AND isset($_POST['description']) AND !empty($_POST['description'])) {
	
	//$idmax = MaxIdnouvelle()+1;
	//$df = $_FILES['image']['name'];
	
	if ($_FILES['image']['error'] <= 0) //virification de l'emage
	{
	
		/**INSERTION A LA BASE**/
		if(TrouvTitre($_POST['titre'])==0) {
			InsertNouvelle($_POST['categorie'],$_POST['sous_categorie'],$_POST['auteur'],$_POST['titre'],$_POST['description'],0);
			/**UPLOAD DE L'IMAGE**/
			$extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  ); //extention de l'image "jpg"
			if($extension_upload=="jpg") {
				$idmax = MaxIdnouvelle();
				$nom = "../nouvelle/{$idmax}.{$extension_upload}";
				$nom = iconv('UTF-8', 'CP1252', $nom);
				$resultat = move_uploaded_file($_FILES['image']['tmp_name'],$nom); //upload de l'image
				$valide = "nouvelle inserer avec succes";
			} else {
				$erreur = "choisi un fichier image2";
			}
		} else{
			$erreur = "titre existant";
		}
	} else {
		
		$erreur = "choisi un fichier image";
	}
	/**$extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  );
	$nom = "../nouvelle/{$idmax}.{$extension_upload}";
	$nom = iconv('UTF-8', 'CP1252', $nom);
	$resultat = move_uploaded_file($_FILES['image']['tmp_name'],$nom);**/
}
else if(isset($_POST['categorie']) AND $_POST['categorie']=="Tout"  or isset($_POST['sous_categorie']) AND $_POST['sous_categorie']=="Tout" or isset($_POST['titre']) AND empty($_POST['titre']) or isset($_POST['description']) AND empty($_POST['description'])) {
	$erreur="Tout les champs sont obligatoire";
} ?>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MAGNEWS: Admin - Ajouter nouvelle</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">MAGNEWS Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>Onintsoa<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="deconnect.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                     <li>
                        <a href="suprimer.php"><i class="fa fa-fw fa-table"></i> Suprimer nouvelle</a>
                    </li>
                    <li  class="active">
                        <a href="ajouter.php"><i class="fa fa-fw fa-edit"></i> Ajouter nouvelle</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Ajouter Nouvelle
                        </h1>
                    </div>
                </div>
                <!-- /.row -->
				<div class="row">
					<div class="col-lg-1">
						
					</div>
					<div class="col-lg-10">
						<form method="POST" name="ajout_nouvelle" enctype="multipart/form-data">
							
							<div class="form-group">
								<label>Categorie</label>
								<select name="categorie" class="form-control" id="rp1">
									<option value="Tout">Categorie</option>
									<?php $req = ListCategorie(); while ($donnees = $req->fetch()) { ?>
											<option value="<?php  echo $donnees['id_categorie'] ?>"><?php echo  $donnees['categorie']; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label>Sous categorie</label>
								<select name="sous_categorie" class="form-control" id="rp2">
									<option value="Tout">Sous categorie</option>
								</select>
							</div>
							 <div class="form-group">
                                <label>Image</label>
                                <input type="file"  name="image"/>
                            </div>
							 <div class="form-group">
                                <label>Auteur</label>
                                <input name="auteur" class="form-control"/>
                            </div>
							<div class="form-group">
                                <label>Titre</label>
                                <input name="titre" class="form-control"/>
                            </div>
							<div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control" rows="3"></textarea>
                            </div>
                              <button type="submit" onclick="ajout_nouvelle.submit()" class="btn btn-primary">AJOUTER</button>
                           
						</form>
						
						<div class="form-group has-error">
                                <label class="control-label" for="inputError"><?php if(isset($erreur)) { echo $erreur;  } ?></label>
                              
                            </div>
							<div class="form-group has-success">
                                <label class="control-label" for="inputSuccess"><?php if(isset($valide)) { echo $valide;  } ?></label>
                            </div>
					</div>
					<div class="col-lg-1"></div>
				</div>
               
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
<script>
	var rep1 = document.getElementById('rp1');
	var rep2 = document.getElementById('rp2');
	
	rep1.addEventListener('change', function() {
		if(rep1.options[rep1.selectedIndex].innerHTML!='categorie')
		{
			<?php $req_categorie = ListCategorie(); while ($donnees = $req_categorie->fetch()) { ?>						
			if ((rep1.options[rep1.selectedIndex].innerHTML)=="<?php echo  $donnees['categorie']; ?>") 
			{	
			<?php $req_sous_categorie=ListSouscategorie($donnees['id_categorie']); ?>
			rep2.innerHTML='<?php while ($donnees = $req_sous_categorie->fetch()) { ?><option value="<?php echo  $donnees['id_sous_categorie']; ?>"><?php echo  $donnees['sous_categorie']; ?></option><?php } ?>';
			}
			<?php } ?>
		
		} else {
			rep2.innerHTML='<option value="Tout">Sous categorie</option>';

		}
	}, true); 
			
</script>
