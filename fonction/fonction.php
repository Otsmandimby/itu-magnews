<?php
	function url() {
		return '/ITU-template-magnews/';
	}
	function RacineHref() {
		return '<base href="'.url().'">';
	}
	function premiers_mots($nombre,$texte)  
	{  
		  $tabTexte = explode(" ", $texte);
			$NouveauTexte="";
		  for($i=0; $i<$nombre; $i++)  
		  {  
			   $NouveauTexte .= " ".$tabTexte[$i];  
		  }
		  return $NouveauTexte;  
	} 
	function DateMoiAnsJour($date) {
		
		$date = date_parse($date);
		if($date['month']==1) {
			$mois = "Janvier";
		}
		if($date['month']==2) {
			$mois = "Fevrier";
		}
		if($date['month']==3) {
			$mois = "Mars";
		}
		if($date['month']==4) {
			$mois = "Avril";
		}
		if($date['month']==5) {
			$mois = "Mais";
		}
		if($date['month']==6) {
			$mois = "Juin";
		}
		if($date['month']==7) {
			$mois = "Juillet";
		}
		if($date['month']==8) {
			$mois = "Aout";
		}
		if($date['month']==9) {
			$mois = "Septembre";
		}
		if($date['month']==10) {
			$mois = "Octobre";
		}
		if($date['month']==11) {
			$mois = "Novembre";
		}
		if($date['month']==12) {
			$mois = "Decembre";
		}
		$nouveau_date = $date['day']." ".$mois." ".$date['year'];
		return $nouveau_date;
	}
	function MaxIdnouvelle() {
		include("connect_pdo.php");
		$reponse = $bdd->query('SELECT MAX(id_nouvelle) AS max_id_nouvelle FROM nouvelle');//requete
		$donnees = $reponse->fetch();
		$max_id_nouvelle = $donnees['max_id_nouvelle'];
		$reponse->closeCursor();
		return $max_id_nouvelle;
	}
	function InsertNouvelle($id_categorie,$id_sous_categorie,$auteur,$titre,$description,$vue) {
		include("connect_pdo.php");
		$req = $bdd->prepare('INSERT INTO nouvelle(id_categorie, id_sous_categorie, auteur,titre,description,vue,date_nouvelle) VALUES(:id_categorie, :id_sous_categorie, :auteur,:titre,:description,:vue,NOW())');//NOW()
		$req->execute(array(
		'id_categorie' => $id_categorie,
		'id_sous_categorie' => $id_sous_categorie,
		'auteur' => $auteur,
		'titre' => $titre,
		'description' => $description,
		'vue' => $vue	));
		$req->closeCursor();
		return 1;
	}
	function TrouvTitre($titre) {
		include("connect_pdo.php");
		$req = $bdd->prepare('SELECT COUNT(*) as nb_titre FROM nouvelle WHERE titre = ?'); $req->execute(array($titre));
		$donnees = $req->fetch();
		$nb_joueur=$donnees['nb_titre'];
		$req->closeCursor();
		return $nb_joueur;
	}
	function TrouvNouvelle($id_nouvelle) {
		include("connect_pdo.php");
		$req = $bdd->prepare('SELECT * FROM nouvelle WHERE id_nouvelle = ?'); $req->execute(array($id_nouvelle));
		return $req;
	}
	function TrouvSouscat($id_sous_categorie) {
		include("connect_pdo.php");
		$req = $bdd->prepare('SELECT * FROM sous_categorie WHERE id_sous_categorie = ?'); $req->execute(array($id_sous_categorie));
		$donnees = $req->fetch();
		$sous_categorie=$donnees['sous_categorie'];
		$req->closeCursor();
		return $sous_categorie;
	}
	function ListCategorie() {
		include("connect_pdo.php");
		$reponse = $bdd->query('SELECT * FROM categorie');//requete
		/**while ($donnees = $reponse->fetch()) //$donnees = $reponse->fetch();
		{
			echo $donnees['id_joueur'];
		}**/
		return $reponse;
	}
	function ListNouvelle() {
		include("connect_pdo.php");
		$reponse = $bdd->query('select * from nouvelle join categorie on categorie.id_categorie=nouvelle.id_categorie join sous_categorie on sous_categorie.id_sous_categorie=nouvelle.id_sous_categorie ORDER BY date_nouvelle desc');//requete
		return $reponse;
	
	}
	function SuprNouvelle($id_nouvelle) {
		include("connect_pdo.php");
		$req = $bdd->prepare('DELETE FROM nouvelle WHERE id_nouvelle = :id_nouvelle'); 
		$req->execute(array(
		'id_nouvelle' => $id_nouvelle ));
		$req->closeCursor();
	}
	function ListSouscategorie($id_categorie) {
		include("connect_pdo.php");
		$req = $bdd->prepare('SELECT * FROM sous_categorie WHERE id_categorie = ?'); $req->execute(array($id_categorie));
		return $req;
	}
	function ListNouvelleLimit($id_categorie,$id_sous_categorie,$limit) {
		include("connect_pdo.php");
		$req = $bdd->prepare('SELECT * FROM nouvelle WHERE id_categorie = ? AND id_sous_categorie = ? LIMIT 0,'.$limit.''); $req->execute(array($id_categorie,$id_sous_categorie));
		return $req;
	}
	function ListNouvelleCtsct($categorie,$sous_categorie) {
		include("connect_pdo.php");
		$req = $bdd->prepare('select * from nouvelle join categorie on categorie.id_categorie=nouvelle.id_categorie join sous_categorie on sous_categorie.id_sous_categorie=nouvelle.id_sous_categorie where categorie=? AND sous_categorie=? ORDER BY date_nouvelle desc');//requete
		$req->execute(array($categorie,$sous_categorie));
		
		return $req;
	}
	
	function ListNouvelleTopLimit($id_categorie,$limit) {
		include("connect_pdo.php");
		$req = $bdd->prepare('SELECT * FROM nouvelle  WHERE id_categorie = ? ORDER BY vue DESC LIMIT 0,'.$limit.''); $req->execute(array($id_categorie));
		return $req;
		//href="Categorie/<?php echo $donneestop['categorie'].'/'.$donneestop['sous_categorie'].'/'.$donneestop['id_nouvelle']; "
	}
	function ListNouvelleNouveau() {
		include("connect_pdo.php");
		$reponse = $bdd->query('SELECT * FROM nouvelle join categorie on categorie.id_categorie = nouvelle.id_categorie join sous_categorie on sous_categorie.id_sous_categorie=nouvelle.id_sous_categorie ORDER BY date_nouvelle');//requete
		return $reponse;
	}
	function ListNouvelleNouveauLimit($limit) {
		include("connect_pdo.php");
		$reponse = $bdd->query('SELECT * FROM nouvelle join categorie on categorie.id_categorie = nouvelle.id_categorie join sous_categorie on sous_categorie.id_sous_categorie=nouvelle.id_sous_categorie ORDER BY vue DESC LIMIT 0,'.$limit.'');//requete
		return $reponse;
	}
	function CountNouvelleCategorie($id_categorie) {
		include("connect_pdo.php");
		$req = $bdd->prepare('select count(*) as nb_nouvelle from nouvelle where id_categorie=?');//requete
		$req->execute(array($id_categorie));
		return $req;
	}
	function CountNouvelle($categorie,$sous_categorie) {
		include("connect_pdo.php");
		$req = $bdd->prepare('select count(*) as nb_nouvelle from nouvelle join categorie on categorie.id_categorie=nouvelle.id_categorie join sous_categorie on sous_categorie.id_sous_categorie=nouvelle.id_sous_categorie where categorie=? AND sous_categorie=?');//requete
		$req->execute(array($categorie,$sous_categorie));
		$donnees = $req->fetch();
		$nb_nouvelle = $donnees['nb_nouvelle'];
		$req->closeCursor();
		return $nb_nouvelle;
	}
	function CountNouvelleId($id_nouvelle){
		include("connect_pdo.php");
		$req = $bdd->prepare('select count(*) as nb_nouvelle from nouvelle where id_nouvelle=?');//requete
		$req->execute(array($id_nouvelle));
		$donnees = $req->fetch();
		$nb_nouvelle = $donnees['nb_nouvelle'];
		$req->closeCursor();
		return $nb_nouvelle;
	}
	function Recherche($recherche) {
		include("connect_pdo.php");
		 $req = $bdd->prepare('SELECT * FROM (SELECT id_nouvelle,categorie,sous_categorie,titre,auteur,description,date_nouvelle FROM nouvelle join categorie on categorie.id_categorie = nouvelle.id_categorie join sous_categorie on sous_categorie.id_sous_categorie=nouvelle.id_sous_categorie ORDER BY date_nouvelle)AS SUBQUERY where categorie like ? OR sous_categorie like ? OR titre like ? OR description like ? limit 0,7'); $req->execute(array('%'.$recherche.'%','%'.$recherche.'%','%'.$recherche.'%','%'.$recherche.'%'));
		return $req;
	}
	function ModifierView($id_nouvelle) {
		include("connect_pdo.php");
		$req = $bdd->prepare('UPDATE nouvelle SET vue = vue+1 WHERE id_nouvelle = ?');
		$req->execute(array($id_nouvelle));
		$req->closeCursor();
	
	}
	function InsertCommentaire($id_nouvelle,$commentaire,$nom,$email) {
		include("connect_pdo.php");
		$req = $bdd->prepare('INSERT INTO commentaire(id_nouvelle, nom, email,commentaire,date_commentaire) VALUES(:id_nouvelle, :nom, :email, :commentaire,NOW())');//NOW()
		$req->execute(array(
		'id_nouvelle' => $id_nouvelle,
		'nom' => $nom,
		'email' => $email,
		'commentaire' => $commentaire ));
		$req->closeCursor();
		return 1;
	}
	function ListCommentaire($id_nouvelle) {
		include("connect_pdo.php");
		$req = $bdd->prepare('select * from commentaire where id_nouvelle=? order by date_commentaire asc');//requete
		$req->execute(array($id_nouvelle));
		return $req;
	}
	function CountCommentaire($id_nouvelle) {
		include("connect_pdo.php");
		$req = $bdd->prepare('select count(*) as nb_commentaire from commentaire where id_nouvelle=?');//requete
		$req->execute(array($id_nouvelle));
		$donnees = $req->fetch();
		$nb_commentaire = $donnees['nb_commentaire'];
		$req->closeCursor();
		return $nb_commentaire;
	}
	

?>