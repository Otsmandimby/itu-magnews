<!DOCTYPE html>
<?php include("fonction/fonction.php"); ?>

<html lang="en">
<head>
	<title>MAGNEWS : Toute l'actualités et infos internationales</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="L 'actualité internationale en continu et les articles de l'hebdomadaire : le meilleur des médias étrangers traduit en français">
		
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" href="concatcss/" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="fonts/fontawesome-5.0.8/css/fontawesome-all.min.css">
</head>
<body class="animsition">
	
	<!--Header-->
		<?php include("include/header.php"); ?>
	<!-- Headline -->
	<div class="container">
		<div class="bg0 flex-wr-sb-c p-rl-20 p-tb-8">
			<div class="f2-s-1 p-r-30 size-w-0 m-tb-6 flex-wr-s-c">
				<!--<span class="text-uppercase cl2 p-r-8">
					Trending Now:
				</span>

				<span class="dis-inline-block cl6 slide100-txt pos-relative size-w-0" data-in="fadeInDown" data-out="fadeOutDown">
					<span class="dis-inline-block slide100-txt-item animated visible-false">
						Interest rate angst trips up US equity bull market
					</span>
					
					<span class="dis-inline-block slide100-txt-item animated visible-false">
						Designer fashion show kicks off Variety Week
					</span>

					<span class="dis-inline-block slide100-txt-item animated visible-false">
						Microsoft quisque at ipsum vel orci eleifend ultrices
					</span>
				</span>-->
			</div>

				<form class="pos-relative size-a-2 bo-1-rad-22 of-hidden bocl11 m-tb-6" action="Recherche" method="POST">
				<input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45" type="text" name="recherche" placeholder="Recherche">
				<button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
					<i class="zmdi zmdi-search"></i>
				</button>
			</form>
		</div>
	</div>
		<?php $req_nouvelle_nouvelle = ListNouvelleNouveau(); $donnees_nouvelle = $req_nouvelle_nouvelle->fetch(); ?>
	<!-- Feature post -->
	<section class="bg0">
		<div class="container">
			<div class="row m-rl--1">
				<div class="col-md-6 p-rl-1 p-b-2">
					<div class="bg-img1 size-a-3 how1 pos-relative" style="background-image: url(nouvelle/<?php echo $donnees_nouvelle['id_nouvelle']; ?>.jpg);">
						<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'].'/'.$donnees_nouvelle['id_nouvelle']; ?> " class="dis-block how1-child1 trans-03"></a>

						<div class="flex-col-e-s s-full p-rl-25 p-tb-20">
							<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'] ?> " class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
								<?php echo $donnees_nouvelle['sous_categorie']; ?>
							</a>

							<h3 class="how1-child2 m-t-14 m-b-10">
								<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'].'/'.$donnees_nouvelle['id_nouvelle']; ?> " class="how-txt1 size-a-6 f1-l-1 cl0 hov-cl10 trans-03">
									<?php echo $donnees_nouvelle['titre']; ?>
								</a>
							</h3>

							<span class="how1-child2">
								<span class="f1-s-4 cl11">
									<?php echo $donnees_nouvelle['auteur']; ?>
								</span>

								<span class="f1-s-3 cl11 m-rl-3">
									-
								</span>

								<span class="f1-s-3 cl11">
									<?php echo DateMoiAnsJour($donnees_nouvelle['date_nouvelle']); ?>
								</span>
							</span>
						</div>
					</div>
				</div>
				<?php  $donnees_nouvelle = $req_nouvelle_nouvelle->fetch(); ?>
				<div class="col-md-6 p-rl-1">
					<div class="row m-rl--1">
						<div class="col-12 p-rl-1 p-b-2">
							<div class="bg-img1 size-a-4 how1 pos-relative" style="background-image: url(nouvelle/<?php echo $donnees_nouvelle['id_nouvelle']; ?>.jpg);">
								<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'].'/'.$donnees_nouvelle['id_nouvelle']; ?> " class="dis-block how1-child1 trans-03"></a>

								<div class="flex-col-e-s s-full p-rl-25 p-tb-24">
									<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'] ?> " class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
										<?php echo $donnees_nouvelle['sous_categorie']; ?>
									</a>

									<h3 class="how1-child2 m-t-14">
										<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'].'/'.$donnees_nouvelle['id_nouvelle']; ?> " class="how-txt1 size-a-7 f1-l-2 cl0 hov-cl10 trans-03">
											<?php echo $donnees_nouvelle['titre']; ?>
										</a>
									</h3>
								</div>
							</div>
						</div>
							<?php  $donnees_nouvelle = $req_nouvelle_nouvelle->fetch(); ?>
						<div class="col-sm-6 p-rl-1 p-b-2">
							<div class="bg-img1 size-a-5 how1 pos-relative" style="background-image:  url(nouvelle/<?php echo $donnees_nouvelle['id_nouvelle']; ?>.jpg);">
								<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'].'/'.$donnees_nouvelle['id_nouvelle']; ?> " class="dis-block how1-child1 trans-03"></a>

								<div class="flex-col-e-s s-full p-rl-25 p-tb-20">
									<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'] ?> " class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
										<?php echo $donnees_nouvelle['sous_categorie']; ?>
									</a>

									<h3 class="how1-child2 m-t-14">
										<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'].'/'.$donnees_nouvelle['id_nouvelle']; ?> " class="how-txt1 size-h-1 f1-m-1 cl0 hov-cl10 trans-03">
											<?php echo $donnees_nouvelle['titre']; ?>
										</a>
									</h3>
								</div>
							</div>
						</div>
							<?php  $donnees_nouvelle = $req_nouvelle_nouvelle->fetch(); ?>
						<div class="col-sm-6 p-rl-1 p-b-2">
							<div class="bg-img1 size-a-5 how1 pos-relative" style="background-image:  url(nouvelle/<?php echo $donnees_nouvelle['id_nouvelle']; ?>.jpg);">
								<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'].'/'.$donnees_nouvelle['id_nouvelle']; ?> " class="dis-block how1-child1 trans-03"></a>

								<div class="flex-col-e-s s-full p-rl-25 p-tb-20">
									<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'] ?> " class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
										<?php echo $donnees_nouvelle['sous_categorie']; ?>
									</a>

									<h3 class="how1-child2 m-t-14">
										<a href="Categorie/<?php echo $donnees_nouvelle['categorie'].'/'.$donnees_nouvelle['sous_categorie'].'/'.$donnees_nouvelle['id_nouvelle']; ?> " class="how-txt1 size-h-1 f1-m-1 cl0 hov-cl10 trans-03">
											<?php echo $donnees_nouvelle['titre']; ?>
										</a>
									</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php $req_nouvelle_nouvelle->closeCursor();  ?>
	<!-- Post -->
	<section class="bg0 p-t-70">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 col-lg-8">
					<div class="p-b-20">
						<?php $req_categorie = ListCategorie(); 
							while ($donnees= $req_categorie->fetch()) //$donnees = $reponse->fetch();
							{
								
							
						?>
						<!-- Entertainment -->
						<div class="tab01 p-b-20">
							<div class="tab01-head how2 how2-cl1 bocl12 flex-s-c m-r-10 m-r-0-sr991">
								<!-- Brand tab -->
								<h3 class="f1-m-2 cl12 tab01-title">
									<?php echo $donnees['categorie']; ?> 
								</h3>

								<!-- Nav tabs -->
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" data-toggle="tab" href="#x<?php echo  $donnees['id_categorie']; ?>" role="tab">Tout</a>
									</li>
									<?php /**LIST SOUS CATEGORIE**/ $tab_id_sc = array();  $req_sous_categorie = ListSouscategorie($donnees['id_categorie']); $nb_sc=1; while ($donnees2 = $req_sous_categorie->fetch()) { $tab_id_sc[$nb_sc]=$donnees2['id_sous_categorie'] ?> 
									<li class="nav-item">
										<a class="nav-link" data-toggle="tab" href="#x<?php echo $donnees['id_categorie'].$donnees2['id_sous_categorie']; ?>" role="tab"><?php echo $donnees2['sous_categorie']; ?> </a>
									</li>
									<?php $nb_sc++; } $req_sous_categorie->closeCursor(); ?>
									<li class="nav-item-more dropdown dis-none">
										<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
											<i class="fa fa-ellipsis-h"></i>
										</a>

										<ul class="dropdown-menu">
											
										</ul>
									</li>
								</ul>

							</div>
								

							<!-- Tab panes -->
							<div class="tab-content p-t-35">
								<!-- - -->
								<div class="tab-pane fade show active" id="x<?php echo  $donnees['id_categorie']; ?>" role="tabpanel">
									<div class="row">
										<?php $req_top_nouvelle = ListNouvelleTopLimit($donnees['id_categorie'],4); 
											$donneestop = $req_top_nouvelle->fetch();
											$sous_categorie1 = TrouvSouscat($donneestop['id_sous_categorie']);											
										?>
										<div class="col-sm-6 p-r-25 p-r-15-sr991">
											<!-- Item post -->	
											<div class="m-b-30">
												<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donneestop['id_nouvelle']; ?> " class="wrap-pic-w hov1 trans-03">
													<img src="nouvelle/<?php echo $donneestop['id_nouvelle']; ?>.jpg" alt="IMG">
												</a>

												<div class="p-t-20">
													<h5 class="p-b-5">
														<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donneestop['id_nouvelle']; ?> " class="f1-m-3 cl2 hov-cl10 trans-03">
															<?php echo $donneestop['titre']; ?>
														</a>
													</h5>

													<span class="cl8">
														<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
															<?php echo $donneestop['auteur'] ?>
														</a>

														<span class="f1-s-3 m-rl-3">
															-
														</span>

														<span class="f1-s-3">
															<?php echo DateMoiAnsJour($donneestop['date_nouvelle']); ?>
														</span>
													</span>
												</div>
											</div>
										</div>

										<div class="col-sm-6 p-r-25 p-r-15-sr991">
											<?php while($donneestop = $req_top_nouvelle->fetch()) { 
												$sous_categorie1 = TrouvSouscat($donneestop['id_sous_categorie']);	
											?>
											<div class="flex-wr-sb-s m-b-30">
												<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donneestop['id_nouvelle']; ?> " class="size-w-1 wrap-pic-w hov1 trans-03">
													<img src="nouvelle/<?php echo $donneestop['id_nouvelle']; ?>.jpg" alt="IMG">
												</a>

												<div class="size-w-2">
													<h5 class="p-b-5">
														<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donneestop['id_nouvelle']; ?> " class="f1-s-5 cl3 hov-cl10 trans-03">
															<?php echo $donneestop['titre']; ?>
														</a>
													</h5>

													<span class="cl8">
														<a href="#" class="f1-s-6 cl8 hov-cl10 trans-03">
															<?php echo $donneestop['auteur'] ?>
														</a>

														<span class="f1-s-3 m-rl-3">
															-
														</span>

														<span class="f1-s-3">
															<?php echo DateMoiAnsJour($donneestop['date_nouvelle']); ?>
														</span>
													</span>
												</div>
											</div>
											<?php } ?>
									</div>
									</div>
								</div>

								<?php for($i=1;$i<=count($tab_id_sc);$i++) { 
									$req_nouvelle = ListNouvelleLimit($donnees['id_categorie'],$tab_id_sc[$i],4);
									$sous_categorie1 = TrouvSouscat($tab_id_sc[$i]);	
								?>
								<div class="tab-pane fade" id="x<?php echo $donnees['id_categorie'].$tab_id_sc[$i]; ?>" role="tabpanel">
									<div class="row">
										<?php $donnees3 = $req_nouvelle->fetch();   ?>
										<div class="col-sm-6 p-r-25 p-r-15-sr991">
											<!-- Item post -->	
											<div class="m-b-30">
												<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donnees3['id_nouvelle']; ?> " class="wrap-pic-w hov1 trans-03">
													<img src="nouvelle/<?php echo $donnees3['id_nouvelle'] ?>.jpg" alt="IMG">
												</a>

												<div class="p-t-20">
													<h5 class="p-b-5">
														<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donnees3['id_nouvelle']; ?> " class="f1-m-3 cl2 hov-cl10 trans-03">
															<?php echo $donnees3['titre'] ?>
														</a>
													</h5>

													<span class="cl8">
														<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
															<?php echo $donnees3['auteur'] ?>
														</a>

														<span class="f1-s-3 m-rl-3">
															-
														</span>

														<span class="f1-s-3">
															<?php echo DateMoiAnsJour($donnees3['date_nouvelle']); ?>
														</span>
													</span>
												</div>
											</div>
										</div>

										<div class="col-sm-6 p-r-25 p-r-15-sr991">
											<!-- Item post -->
										<?php while ($donnees3 = $req_nouvelle->fetch()) {
											$sous_categorie1 = TrouvSouscat($tab_id_sc[$i]);	
										?>
											<div class="flex-wr-sb-s m-b-30">
												<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donnees3['id_nouvelle']; ?> " class="size-w-1 wrap-pic-w hov1 trans-03">
													<img src="nouvelle/<?php echo $donnees3['id_nouvelle'] ?>.jpg" alt="IMG">
												</a>

												<div class="size-w-2">
													<h5 class="p-b-5">
														<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donnees3['id_nouvelle']; ?> " class="f1-s-5 cl3 hov-cl10 trans-03">
															<?php echo $donnees3['titre'] ?>
														</a>
													</h5>

													<span class="cl8">
														<a href="#" class="f1-s-6 cl8 hov-cl10 trans-03">
															<?php echo $donnees3['auteur'] ?>
														</a>

														<span class="f1-s-3 m-rl-3">
															-
														</span>

														<span class="f1-s-3">
															<?php echo DateMoiAnsJour($donnees3['date_nouvelle']); ?>
														</span>
													</span>
												</div>
											</div>
										<?php } $req_nouvelle->closeCursor(); ?>
											<!-- Item post -->	
										</div>
									</div>
								</div>
								<?php } ?>
								<!-- - -->
							</div>
						</div>
						<?php } $req_categorie->closeCursor(); ?>
						</div>
				</div>

				<div class="col-md-10 col-lg-4">
					<div class="p-l-10 p-rl-0-sr991 p-b-20">
						<!--  -->
						<div>
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Les plus populaire
								</h3>
							</div>

							<ul class="p-t-35">
								<?php $nb_top=1; $req_top_nouvelle = ListNouvelleNouveauLimit(5); while ($donnees_topnl = $req_top_nouvelle->fetch()) { //LIST DE CATEGORIE ?>
							
								<li class="flex-wr-sb-s p-b-22">
									<div class="size-a-8 flex-c-c borad-3 size-a-8 bg9 f1-m-4 cl0 m-b-6">
										<?php echo $nb_top; ?>
									</div>

									<a href="Categorie/<?php echo $donnees_topnl['categorie'].'/'.$donnees_topnl['sous_categorie'].'/'.$donnees_topnl['id_nouvelle']; ?> " class="size-w-3 f1-s-7 cl3 hov-cl10 trans-03">
										<?php echo $donnees_topnl['titre']; ?>
									</a>
								</li>
								<?php $nb_top++; } $req_top_nouvelle->closeCursor(); ?>
							</ul>
						</div>

						<!--  -->
						<div class="flex-c-s p-t-8">
							<a href="#">
								<img class="max-w-full" src="images/banner-02.jpg" alt="IMG">
							</a>
						</div>
						
						<!--  -->
						<div class="p-t-50">
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Stay Connected
								</h3>
							</div>

							<ul class="p-t-35">
								<li class="flex-wr-sb-c p-b-20">
									<a href="#" class="size-a-8 flex-c-c borad-3 size-a-8 bg-facebook fs-16 cl0 hov-cl0">
										<span class="fab fa-facebook-f"></span>
									</a>

									<div class="size-w-3 flex-wr-sb-c">
										<span class="f1-s-8 cl3 p-r-20">
											6879 Fans
										</span>

										<a href="#" class="f1-s-9 text-uppercase cl3 hov-cl10 trans-03">
											Like
										</a>
									</div>
								</li>

								<li class="flex-wr-sb-c p-b-20">
									<a href="#" class="size-a-8 flex-c-c borad-3 size-a-8 bg-twitter fs-16 cl0 hov-cl0">
										<span class="fab fa-twitter"></span>
									</a>

									<div class="size-w-3 flex-wr-sb-c">
										<span class="f1-s-8 cl3 p-r-20">
											568 Followers
										</span>

										<a href="#" class="f1-s-9 text-uppercase cl3 hov-cl10 trans-03">
											Follow
										</a>
									</div>
								</li>

								<li class="flex-wr-sb-c p-b-20">
									<a href="#" class="size-a-8 flex-c-c borad-3 size-a-8 bg-youtube fs-16 cl0 hov-cl0">
										<span class="fab fa-youtube"></span>
									</a>

									<div class="size-w-3 flex-wr-sb-c">
										<span class="f1-s-8 cl3 p-r-20">
											5039 Subscribers
										</span>

										<a href="#" class="f1-s-9 text-uppercase cl3 hov-cl10 trans-03">
											Subscribe
										</a>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	

	
	<!-- Footer -->
		<?php include("include/footer.php"); ?>
	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<span class="fas fa-angle-up"></span>
		</span>
	</div>

	<!-- Modal Video 01-->
	<div class="modal fade" id="modal-video-01" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document" data-dismiss="modal">
			<div class="close-mo-video-01 trans-0-4" data-dismiss="modal" aria-label="Close">&times;</div>

			<div class="wrap-video-mo-01">
				<div class="video-mo-01">
					<iframe src="https://www.youtube.com/embed/wJnBTPUQS5A?rel=0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>