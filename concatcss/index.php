<?php
	header("Content-type: text/css; charset=UTF-8");
	ob_start('ob_gzhandler');
	include "../vendor/bootstrap/css/bootstrap.min.css";
	include "../fonts/font-awesome-4.7.0/css/font-awesome.min.css";
	include "../fonts/fontawesome-5.0.8/css/fontawesome-all.min.css";
	include "../fonts/iconic/css/material-design-iconic-font.min.css";
	include "../vendor/animate/animate.css";
	include "../vendor/css-hamburgers/hamburgers.min.css";
	include "../vendor/animsition/css/animsition.min.css";
	include "../css/util.min.css";
	include "../css/main.css";
	ob_end_flush();
?>

	<!--===============================================================================================-->
	<!--<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<!--<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--<link rel="stylesheet" type="text/css" href="fonts/fontawesome-5.0.8/css/fontawesome-all.min.css">
<!--=====================================================================================/////==========-->
	<!--<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<!--<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<!--<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<!--<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<!--<link rel="stylesheet" type="text/css" href="css/util.min.css">
<!--===============================================================================================-->	
	<!--<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->