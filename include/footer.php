	<footer>
		<div class="bg2 p-t-40 p-b-25">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h1><a href="Accueil" style="font-size: 30px; font-weight: bold;"><span style="color:#17b978">MAG</span><span style="color:white">NEWS</span><!--<img src="images/icons/logo-01.png" alt="LOGO">--></a></h1>
				
						</div>

						<div>
							<p class="f1-s-1 cl11 p-b-16">
								 Retrouvez les actualités International en temps réel sur MAGNEWS: Afrique, Amérique latine, Asie & Océanie, Etats-Unis, Europe, Moyen-Orient.
							</p>

							<p class="f1-s-1 cl11 p-b-16">
								Des questions? appelez nous (+1) xx xxx xxxx
							</p>

							<div class="p-t-15">
								<table>
							<tr>
								<td><a href="#"><div style="width: 29px;height: 22px;background: url(sprite/sprite.gif) -3px -3px no-repeat;background-size: 119px;"></div></a></td>
								<td><a href="#"><div style="width: 23px;height: 18px;background: url(sprite/sprite.gif) -34px -5px no-repeat;background-size: 119px;"></div></a></td>
								<td><a href="#"><div style="width: 31px;height: 20px;background: url(sprite/sprite.gif) -60px -5px no-repeat;background-size: 119px;"></div></a></td>
								<td><a href="#"><div style="width: 37px;height: 20px;background: url(sprite/sprite.gif) -91px -5px no-repeat;background-size: 119px;"></div></a></td>
							</tr>
						</table>
							</div>
						</div>
					</div>

					<div class="col-sm-6 col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h5 class="f1-m-7 cl0">
								Popular Posts
							</h5>
						</div>

						<ul>
							<?php $req_top_nouvelle = ListNouvelleNouveauLimit(3); while ($donnees_topnl = $req_top_nouvelle->fetch()) { //LIST DE CATEGORIE ?>
							
							<li class="flex-wr-sb-s p-b-20">
								<a href="Categorie/<?php echo $donnees_topnl['categorie'].'/'.$donnees_topnl['sous_categorie'].'/'.$donnees_topnl['id_nouvelle']; ?> " class="size-w-4 wrap-pic-w hov1 trans-03">
									<img src="nouvelle/<?php echo $donnees_topnl['id_nouvelle']; ?>.jpg" alt="IMG">
								</a>

								<div class="size-w-5">
									<h6 class="p-b-5">
										<a href="Categorie/<?php echo $donnees_topnl['categorie'].'/'.$donnees_topnl['sous_categorie'].'/'.$donnees_topnl['id_nouvelle']; ?> " class="f1-s-5 cl11 hov-cl10 trans-03">
											<?php echo $donnees_topnl['titre']; ?>
										</a>
									</h6>

									<span class="f1-s-3 cl6">
										<?php echo DateMoiAnsJour($donnees_topnl['date_nouvelle']); ?>
									</span>
								</div>
							</li>
							<?PHP } $req_top_nouvelle->closeCursor(); ?>
						</ul>
					</div>

					<div class="col-sm-6 col-lg-4 p-b-20">
						<div class="size-h-3 flex-s-c">
							<h5 class="f1-m-7 cl0">
								Category
							</h5>
						</div>
						<?php $req_categorie = ListCategorie(); while ($donnees = $req_categorie->fetch()) { //LIST DE CATEGORIE

							$req_nb_nouvelle = CountNouvelleCategorie($donnees['id_categorie']);
							$donnees_nbnl = $req_nb_nouvelle->fetch();
						?>
						<ul class="m-t--12">
							<li class="how-bor1 p-rl-5 p-tb-10">
								<a class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
									<?php echo $donnees['categorie']; ?> (<?php echo $donnees_nbnl['nb_nouvelle']; ?>)
								</a>
							</li>
						<?php } ?>
					
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="bg11">
			<div class="container size-h-4 flex-c-c p-tb-15">
				<span class="f1-s-1 cl0 txt-center">
					Copyright 2019

				
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</span>
			</div>
		</div>
	</footer>
