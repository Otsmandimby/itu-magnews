	<!-- Header -->
	
	<header>
		<!-- Header desktop -->
		<div class="container-menu-desktop">
			<div class="topbar">
				<div class="content-topbar container h-100">
					<div class="left-topbar">
						<span class="left-topbar-item flex-wr-s-c">
							<span>
								New York, NY
							</span>

							<img class="m-b-1 m-rl-8" src="images/icons/icon-night.png" alt="IMG">

							<span>
								HI 58° LO 56°
							</span>

						</span>

					</div>

					<div class="right-topbar">
						<table>
							<tr>
								<td><a href="#"><div style="width: 29px;height: 22px;background: url(sprite/sprite.gif) -3px -3px no-repeat;background-size: 119px;"></div></a></td>
								<td><a href="#"><div style="width: 23px;height: 18px;background: url(sprite/sprite.gif) -34px -5px no-repeat;background-size: 119px;"></div></a></td>
								<td><a href="#"><div style="width: 31px;height: 20px;background: url(sprite/sprite.gif) -60px -5px no-repeat;background-size: 119px;"></div></a></td>
								<td><a href="#"><div style="width: 37px;height: 20px;background: url(sprite/sprite.gif) -91px -5px no-repeat;background-size: 119px;"></div></a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>

			<!-- Header Mobile -->
			<div class="wrap-header-mobile">
				<!-- Logo moblie -->		
				<div class="logo-mobile">
					<a href="Accueil"><img src="images/icons/logo-01.png" alt="IMG-LOGO"></a>
				</div>

				<!-- Button show menu -->
				<div class="btn-show-menu-mobile hamburger hamburger--squeeze m-r--8">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</div>
			</div>

			<!-- Menu Mobile -->
			<div class="menu-mobile">
				<ul class="topbar-mobile">
					<li class="left-topbar">
						<span class="left-topbar-item flex-wr-s-c">
							<span>
								New York, NY
							</span>

							<img class="m-b-1 m-rl-8" src="images/icons/icon-night.png" alt="IMG">

							<span>
								HI 58° LO 56°
							</span>
						</span>
					</li>

					<li class="left-topbar">
						<a href="#" class="left-topbar-item">
							About
						</a>

						<a href="#" class="left-topbar-item">
							Contact
						</a>

						<a href="#" class="left-topbar-item">
							Sing up
						</a>

						<a href="#" class="left-topbar-item">
							Log in
						</a>
					</li>

					<li class="right-topbar">
						<a href="#">
							<span class="fab fa-facebook-f"></span>
							<img src="sprite/sprite.jpg">
						</a>

						<a href="#">
							<span class="fab fa-twitter"></span>
						</a>

						<a href="#">
							<span class="fab fa-pinterest-p"></span>
						</a>

						<a href="#">
							<span class="fab fa-vimeo-v"></span>
						</a>

						<a href="#">
							<span class="fab fa-youtube"></span>
						</a>
					</li>
				</ul>

				<ul class="main-menu-m">
					<li>
						<a href="index.php">Home</a>
						<ul class="sub-menu-m">
							<li><a href="index.php">Homepage v1</a></li>
							<li><a href="home-02.html">Homepage v2</a></li>
							<li><a href="home-03.html">Homepage v3</a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</li>

					<li>
						<a href="category-01.html">News</a>
					</li>

					<li>
						<a href="category-02.html">Entertainment </a>
					</li>

					<li>
						<a href="category-01.html">Business</a>
					</li>

					<li>
						<a href="category-02.html">Travel</a>
					</li>

					<li>
						<a href="category-01.html">Life Style</a>
					</li>

					<li>
						<a href="category-02.html">Video</a>
					</li>

					<li>
						<a href="#">Features</a>
						<ul class="sub-menu-m">
							<li><a href="category-01.html">Category Page v1</a></li>
							<li><a href="category-02.html">Category Page v2</a></li>
							<li><a href="blog-grid.html">Blog Grid Sidebar</a></li>
							<li><a href="blog-list-01.html">Blog List Sidebar v1</a></li>
							<li><a href="blog-list-02.html">Blog List Sidebar v2</a></li>
							<li><a href="blog-detail-01.html">Blog Detail Sidebar</a></li>
							<li><a href="blog-detail-02.html">Blog Detail No Sidebar</a></li>
							<li><a href="about.html">About Us</a></li>
							<li><a href="contact.html">Contact Us</a></li>
						</ul>

						<span class="arrow-main-menu-m">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</li>
				</ul>
			</div>
			
			<!--  -->
			<div class="wrap-logo container">
				<!-- Logo desktop -->		
				<div class="logo">
					<h1><a href="Accueil" style="font-size: 30px; font-weight: bold;"><span style="color:#17b978">MAG</span><span style="color:black">NEWS</span><!--<img src="images/icons/logo-01.png" alt="LOGO">--></a></h1>
					
				</div>	

				<!-- Banner -->
				<div class="banner-header">
					<a href="#"><img src="images/banner-01.jpg" alt="IMG">
						
					</a>
				</div>
			</div>	
			
			<!--  -->
			<div class="wrap-main-nav">
				<div class="main-nav">
					<!-- Menu desktop -->
					<nav class="menu-desktop">
						
						<h1 class="logo-stick"><a href="Accueil" style="font-size: 30px; font-weight: bold;"><span style="color:#17b978">MAG</span><span style="color:black">NEWS</span><!--<img src="images/icons/logo-01.png" alt="LOGO">--></a></h1>
				

						<ul class="main-menu">
							
							<?php $req_categorie = ListCategorie(); while ($donnees = $req_categorie->fetch()) { //LIST DE CATEGORIE ?>
											
									
							<li class="mega-menu-item"> <!--class="main-menu-active"-->
								<a href="#" ><?php echo  $donnees['categorie']; ?></a>

								<div class="sub-mega-menu">
									<div class="nav flex-column nav-pills" role="tablist">
										<a class="nav-link active" data-toggle="pill" href="#<?php echo  $donnees['id_categorie']; ?>" role="tab">Tout</a>
										<?php /**LIST SOUS CATEGORIE**/ $tab_id_sc = array();  $req_sous_categorie = ListSouscategorie($donnees['id_categorie']); $nb_sc=1; while ($donnees2 = $req_sous_categorie->fetch()) { $tab_id_sc[$nb_sc]=$donnees2['id_sous_categorie'] ?>
										<a class="nav-link" data-toggle="pill"  href="#<?php echo $donnees['id_categorie'].$donnees2['id_sous_categorie']; ?>" onclick="document.location='Categorie/<?php echo $donnees['categorie']; ?>/<?php echo $donnees2['sous_categorie']; ?>'; return false;" role="tab"><?php echo $donnees2['sous_categorie']; ?></a>
										<?php $nb_sc++; } $req_sous_categorie->closeCursor(); ?>
									</div>

									<div class="tab-content">
										<div class="tab-pane show active" id="<?php echo  $donnees['id_categorie']; ?>" role="tabpanel">
											<div class="row">
											<?php $req_top_nouvelle = ListNouvelleTopLimit($donnees['id_categorie'],4); 
												while ($donneestop = $req_top_nouvelle->fetch()) {
												$sous_categorie1 = TrouvSouscat($donneestop['id_sous_categorie']);
											?>
												<div class="col-3">
													<!-- Item post -->	
													<div>
														<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donneestop['id_nouvelle']; ?> "  class="wrap-pic-w hov1 trans-03">
															<img src="nouvelle/<?php echo $donneestop['id_nouvelle']; ?>.jpg" alt="IMG">
														</a>
														<div class="p-t-10">
															<h5 class="p-b-5">
																<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie1.'/'.$donneestop['id_nouvelle']; ?> " class="f1-s-5 cl3 hov-cl10 trans-03">
																	<?php echo $donneestop['titre']; ?>
																</a>
															</h5>

															<span class="cl8">
																	<?php echo $donneestop['auteur']; ?>

																<span class="f1-s-3 m-rl-3">
																	-
																</span>

																<span class="f1-s-3">
																	<?php echo DateMoiAnsJour($donneestop['date_nouvelle']); ?>
																</span>
															</span>
														</div>
													</div>
												</div>
											<?php } ?>
											</div>
										</div>
										<?php for($i=1;$i<=count($tab_id_sc);$i++) { 
											$req_nouvelle = ListNouvelleLimit($donnees['id_categorie'],$tab_id_sc[$i],4);
											
										?>
										<div class="tab-pane" id="<?php echo $donnees['id_categorie'].$tab_id_sc[$i]; ?>" role="tabpanel">
											<div class="row">
												<?php while ($donnees3 = $req_nouvelle->fetch()) { 
													$sous_categorie2 = TrouvSouscat($donnees3['id_sous_categorie']);
												?>
												<div class="col-3">
													<!-- Item post -->	
													<div>
														<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie2.'/'.$donnees3['id_nouvelle']; ?> " class="wrap-pic-w hov1 trans-03">
															<img src="nouvelle/<?php echo $donnees3['id_nouvelle'] ?>.jpg" alt="IMG">
														</a>

														<div class="p-t-10">
															<h5 class="p-b-5">
																<a href="Categorie/<?php echo $donnees['categorie'].'/'.$sous_categorie2.'/'.$donnees3['id_nouvelle']; ?> " class="f1-s-5 cl3 hov-cl10 trans-03">
																	<?php echo $donnees3['titre']; ?>
																</a>
															</h5>

															<span class="cl8">
																<?php echo $donnees3['auteur']; ?>

																<span class="f1-s-3 m-rl-3">
																	-
																</span>

																<span class="f1-s-3">
																		<?php echo DateMoiAnsJour($donnees3['date_nouvelle']); ?>
																</span>
															</span>
														</div>
													</div>
												</div>
												<?php } ?>
											</div>
										</div>
										<?php 	} ?>
									</div>
								</div>
							</li>
							
							<?php $nb_sc=0; }  $req_categorie->closeCursor(); ?>
						
							
						</ul>
					</nav>
				</div>
			</div>	
		</div>
	</header>
