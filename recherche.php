<?php 
include("fonction/fonction.php"); 
if(isset($_POST['recherche']) AND !empty($_POST['recherche'])) {
	
	$recherche =$_POST['recherche'];
	include("fonction/connect_pdo.php");
	$req_recherche = Recherche($recherche);
	
	
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>MAGNEWS : Recherche</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	 <meta name="description" content="Vous cherchez un article ? Retrouvez toutes les archives de MAGNEWS gr�ce � notre moteur de recherche">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" href="concatcss/" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="fonts/fontawesome-5.0.8/css/fontawesome-all.min.css">
</head>
<body class="animsition">
	
	<!-- Header -->
	<?php include("include/header.php"); ?>
	<!-- Breadcrumb -->
	<div class="container">
		<div class="bg0 flex-wr-sb-c p-rl-20 p-tb-8">
			<div class="f2-s-1 p-r-30 m-tb-6">
				<a href="index.html" class="breadcrumb-item f1-s-3 cl9">
					Home 
				</a>

				<span class="breadcrumb-item f1-s-3 cl9">
					Recherche
				</span>
			</div>

			<form class="pos-relative size-a-2 bo-1-rad-22 of-hidden bocl11 m-tb-6" action="Recherche" method="POST">
				<input class="f1-s-1 cl6 plh9 s-full p-l-25 p-r-45" type="text" name="recherche" placeholder="Recherche">
				<button class="flex-c-c size-a-1 ab-t-r fs-20 cl2 hov-cl10 trans-03">
					<i class="zmdi zmdi-search"></i>
				</button>
			</form>
		</div>
	</div>

	<!-- Page heading -->
	<div class="container p-t-4 p-b-40">
		<h2 class="f1-l-1 cl2">
			Resultat du recherche
		</h2>
	</div>

	<!-- Post -->
	<section class="bg0 p-b-55">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 col-lg-8 p-b-80">
					<div class="p-r-10 p-r-0-sr991">
						<div class="m-t--40 p-b-40">
							<!-- Item post -->
							<?php if(isset($req_recherche)) {
								while ($donnees = $req_recherche->fetch()) {
							?>
							<div class="flex-wr-sb-s p-t-40 p-b-15 how-bor2">
								<a href="Categorie/<?php echo $donnees['categorie'].'/'.$donnees['sous_categorie'].'/'.$donnees['id_nouvelle']; ?> " class="size-w-8 wrap-pic-w hov1 trans-03 w-full-sr575 m-b-25">
									<img src="nouvelle/<?php echo $donnees['id_nouvelle']; ?>.jpg" alt="IMG">
								</a>

								<div class="size-w-9 w-full-sr575 m-b-25">
									<h5 class="p-b-12">
										<a href="Categorie/<?php echo $donnees['categorie'].'/'.$donnees['sous_categorie'].'/'.$donnees['id_nouvelle']; ?> " class="f1-l-1 cl2 hov-cl10 trans-03 respon2">
											<?php echo $donnees['titre']; ?>
										</a>
									</h5>

									<div class="cl8 p-b-18">
										<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
											Par <?php echo $donnees['auteur']; ?>
										</a>

										<span class="f1-s-3 m-rl-3">
											-
										</span>

										<span class="f1-s-3">
											<?php echo DateMoiAnsJour($donnees['date_nouvelle']); ?>
										</span>
									</div>

									<p class="f1-s-1 cl6 p-b-24">
										<?php// echo substr($donnees['description'], 0, 100)."..."; ?>
										
										<?php echo premiers_mots(18,$donnees['description'])."..."; ?>
									</p>

									<a href="Categorie/<?php echo $donnees['categorie'].'/'.$donnees['sous_categorie'].'/'.$donnees['id_nouvelle']; ?> "" class="f1-s-1 cl9 hov-cl10 trans-03">
										Read More
										<i class="m-l-2 fa fa-long-arrow-alt-right"></i>
									</a>
								</div>
							</div>
							<?php } } else { ?>
								<p>Aucun resultat</p>
							
							<?php } ?>
							<!-- Item post -->
						</div>

						
					</div>
				</div>

				<div class="col-md-10 col-lg-4 p-b-80">
					<div class="p-l-10 p-rl-0-sr991">							
						
						<!-- Most Popular -->
						<div class="p-b-23">
							<div class="how2 how2-cl4 flex-s-c">
								<h3 class="f1-m-2 cl3 tab01-title">
									Les plus populaire
								</h3>
							</div>

							<ul class="p-t-35">
								<?php $nb_top=1; $req_top_nouvelle = ListNouvelleNouveauLimit(5); while ($donnees_topnl = $req_top_nouvelle->fetch()) { //LIST DE CATEGORIE ?>
							
								<li class="flex-wr-sb-s p-b-22">
									<div class="size-a-8 flex-c-c borad-3 size-a-8 bg9 f1-m-4 cl0 m-b-6">
										<?php echo $nb_top; ?>
									</div>

									<a href="Categorie/<?php echo $donnees_topnl['categorie'].'/'.$donnees_topnl['sous_categorie'].'/'.$donnees_topnl['id_nouvelle']; ?> " class="size-w-3 f1-s-7 cl3 hov-cl10 trans-03">
										<?php echo $donnees_topnl['titre']; ?>
									</a>
								</li>
								<?php $nb_top++; } $req_top_nouvelle->closeCursor(); ?>
							</ul>
						</div>

						<!--  -->
						<div class="flex-c-s p-b-50">
							<a href="#">
								<img class="max-w-full" src="images/banner-02.jpg" alt="IMG">
							</a>
						</div>
						
				
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Footer -->
	<?php include("include/footer.php"); ?>
	<!-- Back to top -->
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<span class="fas fa-angle-up"></span>
		</span>
	</div>


<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>